import {FC} from "react";
import './ButtonStyles.scss'

type ButtonProps = {
  text: string
  callback: () => void
}

const Button: FC<ButtonProps> = ({text, callback}) => {
  return (
    <button
      className="btn"
      onClick={callback}
    >
      {text}
    </button>
  )
}

export {Button}
