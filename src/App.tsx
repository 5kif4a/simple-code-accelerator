import React, {FC, useEffect, useState} from 'react';
import {Button} from "./components/Button/Button";
import './AppStyles.scss';

const App: FC = () => {
  const [counter, setCounter] = useState<number>(0);
  const [counterBgColor, setCounterBgColor] = useState<string>("#FFFFFF")

  const increment = () => {
    setCounter(counter + 1)
  }

  const decrement = () => {
    setCounter(counter - 1)
  }

  useEffect(() => {
    // https://stackoverflow.com/a/5365036/16560847
    setCounterBgColor("#" + ((1 << 24) * Math.random() | 0).toString(16));

    console.log(counter, "COUNTER", "background-color:", counterBgColor.toUpperCase())
  }, [counter])

  return (
    <div className="app">
      <div className="main">
        <Button text="+" callback={increment}/>

        <div
          className="counter"
          style={{backgroundColor: counterBgColor}}
        >
          {counter}
        </div>

        <Button text="-" callback={decrement}/>
      </div>
    </div>
  );
}

export default App;
